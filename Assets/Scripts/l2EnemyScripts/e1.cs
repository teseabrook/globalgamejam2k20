﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class e1 : MonoBehaviour
{
    int step = 0;
    GameObject player = GameObject.Find("player");
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("player");
    }

    // Update is called once per frame
    void Update()
    {
        if (step == 0)
        {
            this.transform.Translate(0, 0, -0.01f);
            if(this.transform.position.z <= -7)
            {
                step = 1;
            }
           
        }
        else
        {
            this.transform.Translate(0, 0, 0.01f);
            if(this.transform.position.z >= 3)
            {
                step = 0;
            }
        }

        if (Mathf.Abs(this.transform.position.x - player.transform.position.x) <= 1 && Mathf.Abs(this.transform.position.z - player.transform.position.z) <= 1)
        {
            SceneManager.LoadScene("level2");
        }
    }
}
