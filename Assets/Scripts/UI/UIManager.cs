﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class UIManager : MonoBehaviour
{
    Canvas canvas;
    List<GameObject> roboCores;
    List<bool> roboCoreFound;

    // Start is called before the first frame update
    void Start()
    {
        canvas = GetComponent<Canvas>();
        DontDestroyOnLoad(canvas);
        roboCores = new List<GameObject>();
        roboCoreFound = new List<bool>();

        for (int i = 0; i < 5; ++i)
        {
            string robotCoreName = "Robot Core ";
            robotCoreName += (i+1).ToString();
            roboCores.Add(GameObject.Find(robotCoreName));
            roboCoreFound.Add(false);
            roboCores[i].GetComponent<SpriteRenderer>().color = new UnityEngine.Color(1, 1, 1, 0.4f);
        }


       


    }


    // Update is called once per frame
    void Update()
    {
        
    }
}
