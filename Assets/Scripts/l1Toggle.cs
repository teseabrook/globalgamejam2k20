﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class l1Toggle : MonoBehaviour
{
    GameObject player;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("player");
    }

    // Update is called once per frame
    void Update()
    {
        if(this.transform.position.x == player.transform.position.x && this.transform.position.z == player.transform.position.z)
        {
            StaticVars.L1Claimed = true;
        }
    }
}
