﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Room3 : MonoBehaviour
{
    RoomBase roomScript;
    // Start is called before the first frame update
    void Start()
    {
        roomScript = GameObject.Find("room").GetComponent<RoomBase>();
        //Set the variables for the room
        roomScript.setActualPos(new Vector2(-1.5f, -9.5f));
        roomScript.setXMax(12);
        roomScript.setYMax(20);
        //roomScript.setIllegalRoom(new Vector2(5, 0));

        roomScript.setWarpTile(new Vector2(-1, 4));
        roomScript.setWarpRoom(2);
        roomScript.setWarpTile(new Vector2(-1, 5));
        roomScript.setWarpRoom(2);
        roomScript.setWarpTile(new Vector2(-1, 6));
        roomScript.setWarpRoom(2);
        roomScript.setWarpTile(new Vector2(-1, 7));
        roomScript.setWarpRoom(2);

        roomScript.setWarpTile(new Vector2(5, -1));
        roomScript.setWarpRoom(4);
        roomScript.setWarpTile(new Vector2(6, -1));
        roomScript.setWarpRoom(4);
        roomScript.setWarpTile(new Vector2(7, -1));
        roomScript.setWarpRoom(4);
        roomScript.setWarpTile(new Vector2(8, -1));
        roomScript.setWarpRoom(4);
    }

    // Update is called once per frame
    void Update()
    {

    }
}