﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EnemyMoveScript : MonoBehaviour
{
    GameObject player;
    //Move Forward until game is completed
    void Start()
    {
        player = GameObject.Find("player");
    }

    // Update is called once per frame
    void Update()
    {
        if (this.transform.position.x <= 14)
        {
            //Check to see if we impact the player
            if(Mathf.Abs(this.transform.position.x - player.transform.position.x) <= 1 && Mathf.Abs(this.transform.position.z - player.transform.position.z) <= 1)
            {
                SceneManager.LoadScene("level1");
            }

            //Debug.Log(this.transform.position.x);
            this.transform.Translate(new Vector3(0, 0, 0.01f));
        }
    }
}
