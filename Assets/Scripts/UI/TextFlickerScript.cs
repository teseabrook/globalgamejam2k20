﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextFlickerScript : MonoBehaviour
{

    Text text;
    float transparency = 1.0f;
    UnityEngine.Color textColor;
    bool dimming = true;

    // Start is called before the first frame update
    void Start()
    {
        text = GetComponent<Text>();
        textColor = text.color;
    }

    // Update is called once per frame
    void Update()
    {
        if (dimming)
        {
            transparency += Time.deltaTime * 0.65f;
            if (transparency >= 1.0f)
            {
                dimming = false;
            }
        }
        else
        {
            transparency -= Time.deltaTime * 0.65f;
            if (transparency <= 0.0f)
            {
                dimming = true;
            }
        }
       

        text.color = new UnityEngine.Color(text.color.r, text.color.g, text.color.b, transparency);
    }
}
