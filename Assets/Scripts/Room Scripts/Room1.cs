﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Room1 : MonoBehaviour
{
    RoomBase roomScript;
    // Start is called before the first frame update
    void Start()
    {
        roomScript = GameObject.Find("room").GetComponent<RoomBase>();
        //Set the variables for the room
        roomScript.setActualPos(new Vector2(-2.5f, -9.5f));
        roomScript.setXMax(17);
        roomScript.setYMax(22);
        //roomScript.setIllegalRoom(new Vector2(5, 0));

        //Warp Tiles
        roomScript.setWarpTile(new Vector2(11, -1));
        roomScript.setWarpRoom(2);
        roomScript.setWarpTile(new Vector2(12, -1));
        roomScript.setWarpRoom(2);
        roomScript.setWarpTile(new Vector2(13, -1));
        roomScript.setWarpRoom(2);
        roomScript.setWarpTile(new Vector2(14, -1));
        roomScript.setWarpRoom(2);

        roomScript.setWarpTile(new Vector2(18, 11));
        roomScript.setWarpRoom(0);
        roomScript.setWarpTile(new Vector2(18, 12));
        roomScript.setWarpRoom(0);
        roomScript.setWarpTile(new Vector2(18, 13));
        roomScript.setWarpRoom(0);
        roomScript.setWarpTile(new Vector2(18, 14));
        roomScript.setWarpRoom(0);

        roomScript.addMObj("Cube", new Vector2(13, 20));
        roomScript.setStartingTile(new Vector2(15, 12));
        roomScript.setStartingPosition(new Vector2(12.5f, 2.5f));

    }

    // Update is called once per frame
    void Update()
    {

    }
}
