﻿public static class StaticVars
{
    //A group of static variables to be accessed between scenes

    private static bool l0Complete, l0Claimed, l1Complete, l1Claimed, l2Complete, l2Claimed, l3Complete, l3Claimed, l4Complete, l4Claimed = false;

    public static bool L0Complete
    {
        get
        {
            return l0Complete;
        }
        set
        {
            l0Complete = value;
        }
    }

    public static bool L0Claimed
    {
        get
        {
            return l0Claimed;
        }
        set
        {
            l0Claimed = value;
        }
    }

    public static bool L1Complete
    {
        get
        {
            return l1Complete;
        }
        set
        {
            l1Complete = value;
        }
    }

    public static bool L1Claimed
    {
        get
        {
            return l1Claimed;
        }
        set
        {
            l1Claimed = value;
        }
    }

    public static bool L2Complete
    {
        get
        {
            return l2Complete;
        }
        set
        {
            l2Complete = value;
        }
    }

    public static bool L2Claimed
    {
        get
        {
            return l2Claimed;
        }
        set
        {
            l2Claimed = value;
        }
    }


    public static bool L3Complete
    {
        get
        {
            return l3Complete;
        }
        set
        {
            l3Complete = value;
        }
    }

    public static bool L3Claimed
    {
        get
        {
            return l3Claimed;
        }
        set
        {
            l3Claimed = value;
        }
    }

    public static bool L4Complete
    {
        get
        {
            return l3Complete;
        }
        set
        {
            l3Complete = value;
        }
    }

    public static bool L4Claimed
    {
        get
        {
            return l4Claimed;
        }
        set
        {
            l4Claimed = value;
        }
    }



}