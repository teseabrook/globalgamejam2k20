﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Room2 : MonoBehaviour
{
    RoomBase roomScript;
    // Start is called before the first frame update
    void Start()
    {
        roomScript = GameObject.Find("room").GetComponent<RoomBase>();
        //Set the variables for the room
        roomScript.setActualPos(new Vector2(-3.5f, -8.5f));
        roomScript.setXMax(17);
        roomScript.setYMax(20);
        //roomScript.setIllegalRoom(new Vector2(5, 0));


        //Warp Rooms
        roomScript.setWarpTile(new Vector2(-1, 5));
        roomScript.setWarpRoom(0);
        roomScript.setWarpTile(new Vector2(-1, 6));
        roomScript.setWarpRoom(0);
        roomScript.setWarpTile(new Vector2(-1, 7));
        roomScript.setWarpRoom(0);
        roomScript.setWarpTile(new Vector2(-1, 8));
        roomScript.setWarpRoom(0);

        roomScript.setWarpTile(new Vector2(10, 21));
        roomScript.setWarpRoom(1);
        roomScript.setWarpTile(new Vector2(11, 21));
        roomScript.setWarpRoom(1);
        roomScript.setWarpTile(new Vector2(12, 21));
        roomScript.setWarpRoom(1);
        roomScript.setWarpTile(new Vector2(13, 21));
        roomScript.setWarpRoom(1);

        roomScript.setStartingTile(new Vector2(13, 18));
        roomScript.setStartingPosition(new Vector2(9.5f, 10));

        roomScript.setIllegalRoom(new Vector2(13, 16));
        roomScript.setIllegalRoom(new Vector2(13, 15));
        roomScript.setIllegalRoom(new Vector2(13, 14));
        roomScript.setIllegalRoom(new Vector2(12, 16));
        roomScript.setIllegalRoom(new Vector2(12, 15));
        roomScript.setIllegalRoom(new Vector2(12, 14));

        roomScript.setIllegalRoom(new Vector2(9, 16));
        roomScript.setIllegalRoom(new Vector2(9, 15));
        roomScript.setIllegalRoom(new Vector2(9, 14));
        roomScript.setIllegalRoom(new Vector2(8, 16));
        roomScript.setIllegalRoom(new Vector2(8, 15));
        roomScript.setIllegalRoom(new Vector2(8, 14));

        roomScript.setIllegalRoom(new Vector2(0, 16));
        roomScript.setIllegalRoom(new Vector2(0, 15));
        roomScript.setIllegalRoom(new Vector2(1, 16));
        roomScript.setIllegalRoom(new Vector2(1, 15));
        roomScript.setIllegalRoom(new Vector2(2, 16));
        roomScript.setIllegalRoom(new Vector2(2, 15));
        roomScript.setIllegalRoom(new Vector2(3, 16));
        roomScript.setIllegalRoom(new Vector2(3, 15));
        roomScript.setIllegalRoom(new Vector2(4, 16));
        roomScript.setIllegalRoom(new Vector2(4, 15));
        roomScript.setIllegalRoom(new Vector2(5, 16));
        roomScript.setIllegalRoom(new Vector2(5, 15));
        roomScript.setIllegalRoom(new Vector2(15, 10));
        roomScript.setIllegalRoom(new Vector2(15, 11));
        roomScript.setIllegalRoom(new Vector2(16, 10));
        roomScript.setIllegalRoom(new Vector2(16, 11));
        roomScript.setIllegalRoom(new Vector2(17, 10));
        roomScript.setIllegalRoom(new Vector2(17, 11));

        roomScript.setIllegalRoom(new Vector2(5, 9));
        roomScript.setIllegalRoom(new Vector2(5, 10));
        roomScript.setIllegalRoom(new Vector2(4, 9));
        roomScript.setIllegalRoom(new Vector2(4, 10));
        roomScript.setIllegalRoom(new Vector2(3, 9));
        roomScript.setIllegalRoom(new Vector2(3, 10));
        roomScript.setIllegalRoom(new Vector2(2, 9));
        roomScript.setIllegalRoom(new Vector2(2, 10));
        roomScript.setIllegalRoom(new Vector2(1, 9));
        roomScript.setIllegalRoom(new Vector2(1, 10));
        roomScript.setIllegalRoom(new Vector2(0, 9));
        roomScript.setIllegalRoom(new Vector2(0, 10));

        roomScript.setIllegalRoom(new Vector2(15, 7));
        roomScript.setIllegalRoom(new Vector2(15, 6));
        roomScript.setIllegalRoom(new Vector2(16, 7));
        roomScript.setIllegalRoom(new Vector2(16, 6));
        roomScript.setIllegalRoom(new Vector2(17, 7));
        roomScript.setIllegalRoom(new Vector2(17, 6));

        roomScript.setIllegalRoom(new Vector2(12, 8));
        roomScript.setIllegalRoom(new Vector2(12, 7));
        roomScript.setIllegalRoom(new Vector2(12, 6));
        roomScript.setIllegalRoom(new Vector2(11, 8));
        roomScript.setIllegalRoom(new Vector2(11, 7));
        roomScript.setIllegalRoom(new Vector2(11, 6));
        roomScript.setIllegalRoom(new Vector2(10, 8));
        roomScript.setIllegalRoom(new Vector2(10, 7));
        roomScript.setIllegalRoom(new Vector2(10, 6));

        roomScript.setIllegalRoom(new Vector2(12, 2));
        roomScript.setIllegalRoom(new Vector2(11, 2));
        roomScript.setIllegalRoom(new Vector2(12, 1));
        roomScript.setIllegalRoom(new Vector2(11, 1));
        roomScript.setIllegalRoom(new Vector2(12, 0));
        roomScript.setIllegalRoom(new Vector2(11, 0));

        roomScript.setIllegalRoom(new Vector2(7, 2));
        roomScript.setIllegalRoom(new Vector2(8, 2));
        roomScript.setIllegalRoom(new Vector2(7, 1));
        roomScript.setIllegalRoom(new Vector2(8, 1));
        roomScript.setIllegalRoom(new Vector2(7, 0));
        roomScript.setIllegalRoom(new Vector2(8, 0));

        roomScript.setIllegalRoom(new Vector2(3, 2));
        roomScript.setIllegalRoom(new Vector2(4, 2));
        roomScript.setIllegalRoom(new Vector2(3, 1));
        roomScript.setIllegalRoom(new Vector2(4, 1));
        roomScript.setIllegalRoom(new Vector2(3, 0));
        roomScript.setIllegalRoom(new Vector2(4, 0));

    }

    // Update is called once per frame
    void Update()
    {

    }
}
