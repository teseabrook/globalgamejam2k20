﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PauseButtonScript : MonoBehaviour
{
    public Button button;
    bool timePaused = false;
    Text pausedText;

    // Start is called before the first frame update
    void Start()
    {
        button = GetComponent<Button>();
        button.onClick.AddListener(PauseTime);
        pausedText = GameObject.Find("Paused Text").GetComponent<Text>();
        pausedText.color = new UnityEngine.Color(0, 0, 0, 0);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void PauseTime()
    {
        timePaused = !timePaused;
        if (timePaused)
        {
            Debug.Log("Pausing Time");
            Time.timeScale = 0;
            pausedText.color = new UnityEngine.Color(1, 1, 1, 1);
        }
        else
        {
            Debug.Log("Un-Pausing Time");
            Time.timeScale = 1;
            pausedText.color = new UnityEngine.Color(0, 0, 0, 0);
        }
    }
}
