﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerVars : MonoBehaviour
{
    int currentRoom;
    Vector2 currentTile, currentPosition;
    RoomBase roomScript;
    GameObject player;
    bool f1 = false;
    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("dab");
        roomScript = GameObject.Find("room").GetComponent<RoomBase>();
        player = GameObject.Find("player");
        currentRoom = 0;
        //currentTile = new Vector2(0, 0);
       
    }

    // Update is called once per frame
    void Update()
    {
        if(!f1)
        {
            Vector4 starter = roomScript.getStartingPosition();
            currentPosition = new Vector2(starter.x, starter.y);
            currentTile = new Vector2(starter.z, starter.w);
            Debug.Log(currentTile.x + ", " + currentTile.y);
            f1 = true;
        }

        //If a relevant key is pressed do something
        //otherwise just vibe
        if(Input.GetKeyDown(KeyCode.D))
        {
            //Debug.Log("debug: " + (currentTile.x - 1) + "hi" +  currentTile.y);
            int res = roomScript.checkTile((int)currentTile.x - 1, (int)currentTile.y);
            if(res == 2)
            {
                int warp = roomScript.getWarpRoom(new Vector2((int)currentTile.x - 1, (int)currentTile.y));
                currentRoom = warp;
                SceneManager.LoadScene("level" + warp);
                //SceneManager.LoadScene
            }
            else if (res == 3)
            {
                int move = roomScript.getMoveable(new Vector2((int)currentTile.x - 1, (int)currentTile.y));
                roomScript.updateMObj(move, 1);
            }
            else if (res == 0)
            {
                Debug.Log("dabbing to " + (currentTile.x - 1) + ", " + currentTile.y);
                currentTile.x--;

                currentPosition.x--;

                //Debug.Log("r");
                player.transform.eulerAngles = new Vector3(0, 270, 0);

                player.transform.Translate(new Vector3(0, 0, 1));
            }
        }
        else if (Input.GetKeyDown(KeyCode.W))
        {
            int res = roomScript.checkTile((int)currentTile.x, (int)currentTile.y - 1);
            if (res == 2)
            {
                int warp = roomScript.getWarpRoom(new Vector2((int)currentTile.x, (int)currentTile.y - 1));
                currentRoom = warp;
                SceneManager.LoadScene("level" + warp);
            }
            else if (res == 3)
            {
                int move = roomScript.getMoveable(new Vector2((int)currentTile.x, (int)currentTile.y - 1));
                roomScript.updateMObj(move, 3);
            }
            else if (res == 0)
            {
                Debug.Log("dabbing to " + currentTile.x + ", " + (currentTile.y - 1));
                currentTile.y--;

                currentPosition.y--;

                player.transform.eulerAngles = new Vector3(0, 180, 0);

                player.transform.Translate(new Vector3(0, 0, 1));
            }
        }
        else if(Input.GetKeyDown(KeyCode.A))
        {
            int res = roomScript.checkTile((int)currentTile.x + 1, (int)currentTile.y);
            if (res == 2)
            {
                int warp = roomScript.getWarpRoom(new Vector2((int)currentTile.x + 1, (int)currentTile.y));
                currentRoom = warp;
                SceneManager.LoadScene("level" + warp);
            }
            else if (res == 3)
            {
                int move = roomScript.getMoveable(new Vector2((int)currentTile.x + 1, (int)currentTile.y));
                roomScript.updateMObj(move, 0);
            }
            else if (res == 0)
            {
                Debug.Log("dabbing to " + (currentTile.x + 1) + ", " + currentTile.y);
                currentTile.x++;

                currentPosition.x++;

                player.transform.eulerAngles = new Vector3(0, 90, 0);
                player.transform.Translate(new Vector3(0, 0, 1));
            }
        }
        else if (Input.GetKeyDown(KeyCode.S))
        {
            int res = roomScript.checkTile((int)currentTile.x, (int)currentTile.y + 1);
            if (res == 2)
            {
                int warp = roomScript.getWarpRoom(new Vector2((int)currentTile.x, (int)currentTile.y + 1));
                currentRoom = warp;
                SceneManager.LoadScene("level" + warp);
            }
            else if (res == 3)
            {
                int move = roomScript.getMoveable(new Vector2((int)currentTile.x, (int)currentTile.y + 1));
                roomScript.updateMObj(move, 2);
            }
            else if (res == 0)
            {
                Debug.Log("dabbing to " + currentTile.x + ", " + (currentTile.y + 1));
                currentTile.y++;

                currentPosition.y++;
                player.transform.eulerAngles = new Vector3(0, 0, 0);

                player.transform.Translate(new Vector3(0, 0, 1));
            }
        }
        else if (Input.GetKeyDown(KeyCode.E))
        {
            //Interact
        }
    }
}
