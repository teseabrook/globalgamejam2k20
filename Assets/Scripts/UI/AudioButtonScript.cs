﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AudioButtonScript : MonoBehaviour
{
    public Button button;
    bool audioMuted = false;

    // Start is called before the first frame update
    void Start()
    {
        button = GetComponent<Button>();
        button.onClick.AddListener(MuteAudio);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void MuteAudio()
    {
        audioMuted = !audioMuted;
        if (audioMuted)
        {
            UnityEngine.AudioListener.pause = true;
        }
        else
        {
            UnityEngine.AudioListener.pause = false;
        }
    }
}
