﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading;
using UnityEngine.SceneManagement;

public class e0 : MonoBehaviour
{
    int steps = 0;
    // Start is called before the first frame update

    GameObject player;// = GameObject.Find("player");

    void Start()
    {
        player = GameObject.Find("player");
    }

    // Update is called once per frame
    void Update()
    {
        if (steps == 0)
        {
            this.transform.Translate(0, 0, -0.01f);
            if(this.transform.position.z <= 5.5)
            {
                this.transform.Rotate(0, 90, 0);
                steps++;
            }
        }
        else if (steps == 1)
        {
            this.transform.Translate(0, 0, 0.01f);
            if(this.transform.position.x >= 10.5)
            {
                this.transform.Rotate(0, 90, 0);
                steps++;
            }
        }
        else if (steps == 2)
        {
            this.transform.Translate(0, 0, -0.01f);
            if(this.transform.position.z >= 8.5)
            {
                //this.transform.Rotate(0, 180, 0);
                steps++;
            }
        }
        else if (steps == 3)
        {
            this.transform.Translate(0, 0, 0.01f);
            if (this.transform.position.z <= 5.5)
            {
                this.transform.Rotate(0, 90, 0);
                steps++;
            }
        }
        else if (steps == 4)
        {
            this.transform.Translate(0, 0, 0.01f);
            if (this.transform.position.x <= 7.5)
            {
               this.transform.Rotate(0, 90, 0);
                steps++;
            }
        }
        else if (steps == 5)
        {
            this.transform.Translate(0, 0, 0.01f);
            if (this.transform.position.z >= 8.5)
            {
                steps = 0;
            }
        }

        if(Mathf.Abs(this.transform.position.x - player.transform.position.x) <= 1 && Mathf.Abs(this.transform.position.z - player.transform.position.z) <= 1)
        {
            SceneManager.LoadScene("level2");
        }
    }
}
