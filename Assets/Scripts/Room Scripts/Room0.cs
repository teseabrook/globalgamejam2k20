﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Room0 : MonoBehaviour
{
    RoomBase roomScript;
    // Start is called before the first frame update
    void Start()
    {
        roomScript = GameObject.Find("room").GetComponent<RoomBase>();
        //Set the variables for the room
        roomScript.setActualPos(new Vector2(9, 9));
        roomScript.setXMax(20);
        roomScript.setYMax(12);

        //warp door 1
        roomScript.setWarpTile(new Vector2(-1, 4));
        roomScript.setWarpRoom(1);
        roomScript.setWarpTile(new Vector2(-1, 5));
        roomScript.setWarpRoom(1);
        roomScript.setWarpTile(new Vector2(-1, 6));
        roomScript.setWarpRoom(1);
        roomScript.setWarpTile(new Vector2(-1, 7));
        roomScript.setWarpRoom(1);
        //roomScript.setIllegalRoom(new Vector2(5, 0));

        //warp door 2
        roomScript.setWarpTile(new Vector2(9, -1));
        roomScript.setWarpRoom(2);
        roomScript.setWarpTile(new Vector2(10, -1));
        roomScript.setWarpRoom(2);
        roomScript.setWarpTile(new Vector2(11, -1));
        roomScript.setWarpRoom(2);
        roomScript.setWarpTile(new Vector2(12, -1));
        roomScript.setWarpRoom(2);

        roomScript.setIllegalRoom(new Vector2(2, 0));
        roomScript.setIllegalRoom(new Vector2(3, 0));
        roomScript.setIllegalRoom(new Vector2(2, 1));
        roomScript.setIllegalRoom(new Vector2(3, 1));

        roomScript.setIllegalRoom(new Vector2(6, 0));
        roomScript.setIllegalRoom(new Vector2(7, 0));
        roomScript.setIllegalRoom(new Vector2(6, 1));
        roomScript.setIllegalRoom(new Vector2(7, 1));

        roomScript.setIllegalRoom(new Vector2(14, 0));
        roomScript.setIllegalRoom(new Vector2(15, 0));
        roomScript.setIllegalRoom(new Vector2(14, 1));
        roomScript.setIllegalRoom(new Vector2(15, 1));

        roomScript.setIllegalRoom(new Vector2(12, 5));
        roomScript.setIllegalRoom(new Vector2(12, 6));
        roomScript.setIllegalRoom(new Vector2(12, 7));
        roomScript.setIllegalRoom(new Vector2(11, 5));
        roomScript.setIllegalRoom(new Vector2(11, 6));
        roomScript.setIllegalRoom(new Vector2(11, 7));
        roomScript.setIllegalRoom(new Vector2(10, 5));
        roomScript.setIllegalRoom(new Vector2(10, 6));
        roomScript.setIllegalRoom(new Vector2(10, 7));
        roomScript.setIllegalRoom(new Vector2(9, 5));
        roomScript.setIllegalRoom(new Vector2(9, 6));
        roomScript.setIllegalRoom(new Vector2(9, 7));
        roomScript.setIllegalRoom(new Vector2(8, 5));
        roomScript.setIllegalRoom(new Vector2(8, 6));
        roomScript.setIllegalRoom(new Vector2(8, 7));

        roomScript.setIllegalRoom(new Vector2(4, 11));
        roomScript.setIllegalRoom(new Vector2(5, 11));
        roomScript.setIllegalRoom(new Vector2(4, 12));
        roomScript.setIllegalRoom(new Vector2(5, 12));

        roomScript.setIllegalRoom(new Vector2(14, 11));
        roomScript.setIllegalRoom(new Vector2(15, 11));
        roomScript.setIllegalRoom(new Vector2(14, 12));
        roomScript.setIllegalRoom(new Vector2(15, 12));

        roomScript.setStartingPosition(new Vector3(2.5f, 0, 0.5f));
        roomScript.setStartingTile(new Vector2(7, 5));

        // Update is called once per frame
        void Update()
        {

        }
    }
}
