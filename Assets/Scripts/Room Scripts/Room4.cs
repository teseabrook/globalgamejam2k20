﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Room4 : MonoBehaviour
{
    RoomBase roomScript;
    // Start is called before the first frame update
    void Start()
    {
        roomScript = GameObject.Find("room").GetComponent<RoomBase>();
        //Set the variables for the room
        roomScript.setActualPos(new Vector2(-1.5f, -9.5f));
        roomScript.setXMax(12);
        roomScript.setYMax(19);
        //roomScript.setIllegalRoom(new Vector2(5, 0));

        roomScript.setWarpTile(new Vector2(-1, 1));
        roomScript.setWarpRoom(3);
        roomScript.setWarpTile(new Vector2(-1, 2));
        roomScript.setWarpRoom(3);
        roomScript.setWarpTile(new Vector2(-1, 3));
        roomScript.setWarpRoom(3);
        roomScript.setWarpTile(new Vector2(-1, 4));
        roomScript.setWarpRoom(3);

        roomScript.setWarpTile(new Vector2(4, 20));
        roomScript.setWarpRoom(5);
        roomScript.setWarpTile(new Vector2(5, 20));
        roomScript.setWarpRoom(5);
        roomScript.setWarpTile(new Vector2(6, 20));
        roomScript.setWarpRoom(5);
        roomScript.setWarpTile(new Vector2(7, 20));
        roomScript.setWarpRoom(5);
    }

    // Update is called once per frame
    void Update()
    {

    }
}