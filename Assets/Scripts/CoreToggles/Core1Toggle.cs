﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Core1Toggle : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        this.GetComponent<Renderer>().enabled = false;

    }

    // Update is called once per frame
    void Update()
    {
        if (StaticVars.L1Claimed)
        {
            this.GetComponent<Renderer>().enabled = true;
        }
        //transform.rot
    }
}
