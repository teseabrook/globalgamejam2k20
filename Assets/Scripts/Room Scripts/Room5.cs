﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Room5 : MonoBehaviour
{
    RoomBase roomScript;
    // Start is called before the first frame update
    void Start()
    {
        roomScript = GameObject.Find("room").GetComponent<RoomBase>();
        //Set the variables for the room
        roomScript.setActualPos(new Vector2(-8.5f, -8.5f));
        roomScript.setXMax(26);
        roomScript.setYMax(16);
        //roomScript.setIllegalRoom(new Vector2(5, 0));

        roomScript.setWarpTile(new Vector2(10, -1));
        roomScript.setWarpRoom(4);
        roomScript.setWarpTile(new Vector2(11, -1));
        roomScript.setWarpRoom(4);
        roomScript.setWarpTile(new Vector2(12, -1));
        roomScript.setWarpRoom(4);
        roomScript.setWarpTile(new Vector2(13, -1));
        roomScript.setWarpRoom(4);

        roomScript.setWarpTile(new Vector2(15, 17));
        roomScript.setWarpRoom(0);
        roomScript.setWarpTile(new Vector2(16, 17));
        roomScript.setWarpRoom(0);
        roomScript.setWarpTile(new Vector2(17, 17));
        roomScript.setWarpRoom(0);
        roomScript.setWarpTile(new Vector2(18, 17));
        roomScript.setWarpRoom(0);
    }

    // Update is called once per frame
    void Update()
    {

    }
}